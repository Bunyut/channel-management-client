import React from "react";
import { connect } from "react-redux";
import { Icon, Button, Row, Col, Avatar, Layout, Badge } from "antd";
const { Header } = Layout;


const MainHeader = ({
  collapsed,
  toggleCollapsed,
  userInf,
  ...othProps }) => {

  return (
    <Header className="header" style={{ 
        zIndex: 1, 
        width: '100%', 
        background: '#001830', 
        padding: 0 ,
        color:'#fff'
      }} >
      <Icon
        className="trigger"
        type={collapsed ? 'menu-unfold' : 'menu-fold'}
        onClick={toggleCollapsed}
      />
      <div className="logo" ><img  style={{ height: '30px' }} src="/static/images/logo.png" /></div>
      <div className="cartHeader" style={{ float: 'right', marginRight: '50px' }}>
        
        <Avatar icon="user" />
       
        <span style={{ marginLeft: '10px' }}>Nick T.</span>
      </div>
    </Header>
  );
};

const mapStateToProps = ({ styleState }) => {
  return {
    styleState
  };
};

export default connect(mapStateToProps)(MainHeader);
