import React from "react";
import { connect } from "react-redux";
import {Layout, Menu, Breadcrumb, Icon, Button} from "antd";
import { onSetToggleSidebar } from "../../state/actions/StyleAction";
import MainMenu from "./MainMenu";

const {Sider} = Layout;


const MainSidebar = ({isCollapsed, onSetCollapsed}) => {
  return (
    <Sider
      trigger={null}
      collapsible
      collapsed={isCollapsed}
      breakpoint="xl"
      style={{ background: '#fff' }}
      onBreakpoint={(broken) => { console.log('onBreakpoint-->',broken); }}
      onCollapse={(collapsed, type) => { onSetCollapsed(collapsed);console.log('onCollapse-->',collapsed, type); }}
     
    >

      <MainMenu collapsed={isCollapsed} />
    </Sider>
  );
};

const mapStateToProps = (state) => {

	return {
		collapsed:state.styleState.collapeSidebar,

	}
}
//onSetToggleSidebar
export default connect(mapStateToProps, {onSetToggleSidebar})(MainSidebar);
