import React from 'react';
import { Layout, Divider , Icon} from 'antd';
const {
    Header, Footer, Sider, Content,
} = Layout;

const MainFooter = () => {
	return (
		<Footer className="pms-footer" >
			<div style={{fontSize:'30px'}}>
				<Icon type="bar-chart" />
				<Divider type="vertical" />
				<Icon type="stock" />
			</div>
		</Footer>

	);
};

export default MainFooter;