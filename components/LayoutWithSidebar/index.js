import React, { useState } from 'react';
import { Layout,Card } from 'antd';
import MainHeader from "./MainHeader";
import MainSidebar from "./MainSidebar";
import MainFooter from "./MainFooter";

const {
    Header, Footer, Sider, Content
} = Layout;


const LayoutWithSidebar = ({ children }) => {

    const [collapsed, setCollapsed] = useState(false)

    const handleToggleSidebar = () => {
        setCollapsed(!collapsed)
    }


    return (
        <Layout id="sidebar-layout-trigger">
            <MainHeader collapsed={collapsed} toggleCollapsed={handleToggleSidebar} />
            <Layout hasSider style={{ height: '100vh' }} >
                <MainSidebar isCollapsed={collapsed} onSetCollapsed={setCollapsed} />
                <Layout >
                    <Content style={{ padding: "24px 24px 24px" }}>
                        <Card >
                            {children}
                        </Card>
                    </Content>
                    <MainFooter />
                </Layout>

            </Layout>

        </Layout>
    );
};

export default LayoutWithSidebar;