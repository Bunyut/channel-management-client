import React from 'react';
import {
	Form,
	Select,
	InputNumber,
	Switch,
	Radio,
	Slider,
	Button,
	Upload,
	Icon,
	Rate,
	Checkbox,
	Row,
	Col,
} from 'antd';

const { Option } = Select;

const ChannelSettingForm = ({
	form
}) => {

	const { getFieldDecorator } = form;
	const formItemLayout = {
		labelCol: { span: 6 },
		wrapperCol: { span: 14 },
	};


	return (
		<Form {...formItemLayout} >

			<Form.Item label="Enabled">
				{getFieldDecorator('radio-group')(
					<Radio.Group>
						<Radio value="1">Enabled</Radio>
						<Radio value="2">Disabled</Radio>
					</Radio.Group>,
				)}
			</Form.Item>
			<Form.Item label="Channal Currency Conversion">
				<span className="ant-form-text">Conversion not enabled. <Icon type="setting" /></span>
			</Form.Item>
			<Form.Item label="Rate Multiplier">
				<span className="ant-form-text">Rates will not be adjusted before sending. <Icon type="setting" /></span>
			</Form.Item>

			<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
				<Button type="primary" htmlType="submit">Save</Button>
			</Form.Item>
		</Form>
	);
};

export default Form.create({ name: 'ChannelSettingForm' })(ChannelSettingForm);



