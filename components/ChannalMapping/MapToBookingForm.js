import React from 'react';
import {
	Form,
	Select,
	InputNumber,
	Switch,
	Radio,
	Slider,
	Button,
	Upload,
	Icon,
	Rate,
	Checkbox,
	Row,
	Col,
} from 'antd';

const { Option } = Select;

const MapToBookingForm = ({
	form
}) => {

	const { getFieldDecorator } = form;
	const formItemLayout = {
		labelCol: { span: 6 },
		wrapperCol: { span: 14 },
	};


	return (
		<Form {...formItemLayout} >

			<Form.Item label="Map to" hasFeedback>
				{getFieldDecorator('select', {
					rules: [{ required: true, message: 'Please select a room mapping!' }],
				})(
					<Select placeholder="Please select a room mapping">
						<Option value="1">SingleRoom - Advance Purchase</Option>
						<Option value="2">SingleRoom - Standard</Option>
					</Select>,
				)}
			</Form.Item>
			<Form.Item label="Sent">
				<span className="ant-form-text">Rating, Avaliability, Stop sell</span>
			</Form.Item>
			<Form.Item label="No Sent">
				<span className="ant-form-text">Max Stay</span>
			</Form.Item>

			<Form.Item label="Start updating channal">
				{getFieldDecorator('radio-group')(
					<Radio.Group>
						<Radio value="1">Yes</Radio>
						<Radio value="2">No</Radio>
					</Radio.Group>,
				)}
			</Form.Item>


			<Form.Item wrapperCol={{ span: 12, offset: 6 }}>
				<Button type="primary" htmlType="submit">Save</Button>
			</Form.Item>
		</Form>
	);
};

export default Form.create({ name: 'MapToBookingForm' })(MapToBookingForm);