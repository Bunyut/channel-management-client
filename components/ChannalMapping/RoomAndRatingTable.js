import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import { Tabs, Button, Table, Icon, Input } from 'antd';
import Highlighter from 'react-highlight-words';
import { OnLoadRoomRateDatas , OnSetTitleModal ,OnToggleModalModal} from "../../state/actions/RoomAndRateAction";

const RoomAndRatingTable = ({
	datas,
	loading,
	OnLoadRoomRateDatas,
	OnSetTitleModal ,OnToggleModalModal
}) => {

	const [searchText, setSearchText] = useState("");
	const [searchInput, setSearchInput] = useState(null);

	useEffect(() => {
		OnLoadRoomRateDatas();
	}, []);




	const getColumnSearchProps = dataIndex => ({
		filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
			<div style={{ padding: 8 }}>
				<Input
					ref={node => {
						setSearchInput(node);
					}}
					placeholder={`Search ${dataIndex}`}
					value={selectedKeys[0]}
					onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
					onPressEnter={() => handleSearch(selectedKeys, confirm)}
					style={{ width: 188, marginBottom: 8, display: 'block' }}
				/>
				<Button
					type="primary"
					onClick={() => handleSearch(selectedKeys, confirm)}
					icon="search"
					size="small"
					style={{ width: 90, marginRight: 8 }}
				>
					Search
			</Button>
				<Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
					Reset
			</Button>
			</div>
		),
		filterIcon: filtered => (
			<Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
		),
		onFilter: (value, record) =>
			record[dataIndex]
				.toString()
				.toLowerCase()
				.includes(value.toLowerCase()),
		onFilterDropdownVisibleChange: visible => {
			if (visible) {
				setTimeout(() => searchInput.select());
			}
		},
		render: text => (
			<Highlighter
				highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
				searchWords={[searchText]}
				autoEscape
				textToHighlight={text.toString()}
			/>
		),
	});

	const handleSearch = (selectedKeys, confirm) => {
		confirm();
		setSearchText(selectedKeys[0]);
	};

	const handleReset = clearFilters => {
		clearFilters();
		setSearchText('');
	};


	// if(loading && datas.length <= 0){
	// 	return <div>กำลังโหลดข้อมูล...</div>
	// }
	const columns = [


		{
			title: 'Channel Manager Room Type',
			colSpan: 1,
			dataIndex: 'type',
			...getColumnSearchProps('type'),
			render: (value, row, index) => {
				const obj = {
					children: value,
					props: {
						rowSpan: (index % 3 === 0) ? 3 : 0
					}
				};
				return obj;
			},

		},
		{
			title: 'Channel Manager Room Rate',
			dataIndex: 'roomRate',
			...getColumnSearchProps('roomRate')
		},
		{
			title: 'BookingButton Rate',
			dataIndex: 'bookRate',
			...getColumnSearchProps('bookRate'),
			render: (value, row, index) => {

				return <a 
					onClick={()=>{
						OnToggleModalModal(true);
						OnSetTitleModal(`${row.type}/${row.roomRate}/${row.bookRate}`);
					}}
					style={{ fontWeight: 'bold', color: '#055aa6' }}>
					<Icon type="api" style={{paddingRight:'5px'}} />{value}
				</a>
			}
		},
		{
			title: 'Rate Configuration',
			dataIndex: 'rateConfig',
			...getColumnSearchProps('rateConfig')
		},
		{
			title: 'status',
			dataIndex: 'status',
			...getColumnSearchProps('status')
		}
	];


	return (
		<Table
			loading={loading}
			columns={columns}
			dataSource={datas}
			bordered />
	);
};

const mapStateToProps = ({ roomAndRate }) => {

	return {
		datas: roomAndRate.datas,
		loading: roomAndRate.loading

	}
}

export default connect(mapStateToProps, { OnLoadRoomRateDatas,OnSetTitleModal ,OnToggleModalModal })(RoomAndRatingTable);