import React from 'react';
import { connect } from "react-redux";
import { Modal } from 'antd';
import { OnToggleModalModal, OnSetTitleModal } from "../../state/actions/RoomAndRateAction";

const MapToBookingModal = ({ 
  children, 
  title,
  handleCancel,
  visible,
  OnToggleModalModal
}) => {
  return (
    <Modal
    width={800}
      title={title}
      visible={visible}
      onCancel={()=>OnToggleModalModal(false)}
      footer={null}
    >
      {children}
    </Modal>
  );
};


const mapStateToProps = ({roomAndRate}) =>{

  return {
    visible:roomAndRate.modalVisible,
    title:roomAndRate.title,
  }
}

export default connect(mapStateToProps,{OnToggleModalModal, OnSetTitleModal})(MapToBookingModal);