import React from 'react';
import { connect } from "react-redux";
import { Tabs, Button, Table } from 'antd';
const { TabPane } = Tabs;
import RoomAndRatingTable from "./RoomAndRatingTable";
import { OnToggleModalModal, OnSetTitleModal } from "../../state/actions/RoomAndRateAction";
import MapToBookingModal from "./MapToBookingModal";
import MapToBookingForm from "./MapToBookingForm";
import ChannelSettingForm from "./ChannelSettingForm";


const ChannalMapping = () => {

	return (
		<div>
			<div className="card-container">
				<Tabs type="card">
					<TabPane tab="ROOM & RATES" key="1">
						<RoomAndRatingTable />
					</TabPane>
					<TabPane tab="CHANNEL SETTINGS" key="2">
						<ChannelSettingForm />
					</TabPane>
				</Tabs>
			</div>
			<MapToBookingModal >
				<MapToBookingForm />
			</MapToBookingModal>
		</div>
	);
};

export default connect(null, {OnToggleModalModal, OnSetTitleModal})(ChannalMapping);