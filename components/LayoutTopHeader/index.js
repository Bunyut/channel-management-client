import React from 'react';
import { Layout, Menu, Breadcrumb, Icon, Card } from 'antd';

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;
import MainHeader from "./MainHeader";


const LayoutTopHeader = ({ children }) => {
	return (
		<Layout className="layout">
			<MainHeader  />

			<Content style={{ padding: "24px 24px 24px", ackground: '#fff' }}>
				<Card >
					{children}
				</Card>
			</Content>

			<Footer style={{ textAlign: 'center' }}>bedbaker ©2019 Created by bedbaker team</Footer>
		</Layout>
	);
};

export default LayoutTopHeader;