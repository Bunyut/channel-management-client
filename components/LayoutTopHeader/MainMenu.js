import React from 'react';
import Router from 'next/router'
import { Menu, Icon } from 'antd';

const { SubMenu } = Menu;

const MainMenu = () => {

	const goto = (url) => {

		Router.push(url)
	}

	return (
		<Menu
		theme="dark"
        mode="horizontal"
        style={{ lineHeight: '64px',float:'left',marginLeft:'20px' }}
      >
        <Menu.Item key="1">DASHBOARD</Menu.Item>
        <Menu.Item key="2">INVENTORY</Menu.Item>
        <Menu.Item key="3" onClick={()=>goto('/channal_mapping_page')} > ROOM & RATES</Menu.Item>
		<Menu.Item key="4">RESERVATION</Menu.Item>
		<Menu.Item key="5">CHANNELS</Menu.Item>
      </Menu>
	);
};

export default MainMenu;