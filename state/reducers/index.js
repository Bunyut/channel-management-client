import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import StyleReducer from "./StyleReducer";
import RoomAndRateReducer from "./RoomAndRateReducer";

const reducer = combineReducers({
  form: formReducer,
  styleState:StyleReducer,
  roomAndRate:RoomAndRateReducer
})

export default reducer