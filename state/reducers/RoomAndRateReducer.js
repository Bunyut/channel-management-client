import { createAction, createReducer } from 'redux-act';

const init = {

  datas:[],
  loading:true,
  modalVisible:false,
  title:""
}

export const setData = createAction('setData');
export const setLoading = createAction('setLoading');
export const setModalVisible = createAction('setModalVisible');
export const setTitle = createAction('setTitle');

//reducer
export default createReducer({

    [setData]: (state, payload) =>    { return {...state, datas:payload}},
	[setLoading]: (state, payload) =>    { return {...state, loading:payload}},
	[setModalVisible]: (state, payload) =>    { return {...state, modalVisible:payload}},
	[setTitle]: (state, payload) =>    { return {...state, title:payload}}

}, init);