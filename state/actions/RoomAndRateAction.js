import 'isomorphic-unfetch';
import { 
	setData,
	setLoading,
	setModalVisible,
	setTitle
} from "../reducers/RoomAndRateReducer";


export const OnLoadRoomRateDatas = (params) =>{

	return async (dispatch) =>{

		dispatch(setLoading(true));
		const data = [
			{
				key: '1',
				type: 'Single Room',
				roomRate: 'standard',
				bookRate: 'Map to BookingButton',
				rateConfig:'',
				status:''
			},
			{
				key: '2',
				type: 'Single Room',
				roomRate: 'Nono Refundable',
				bookRate: 'Map to BookingButton',
				rateConfig:'',
				status:''
			},
			{
				key: '3',
				type: 'Single Room',
				roomRate: 'Advanced purchace',
				bookRate: 'Map to BookingButton',
				rateConfig:'',
				status:''
			},
			// {
			// 	key: '4',
			// 	type: 'Single Room',
			// 	roomRate: 'Nono Refundable',
			// 	bookRate: 'Map to BookingButton',
			// 	rateConfig:'',
			// 	status:''
			// },
			// {
			// 	key: '5',
			// 	type: 'Single Room',
			// 	roomRate: 'Advanced purchace',
			// 	bookRate: 'Map to BookingButton',
			// 	rateConfig:'',
			// 	status:''
			// }
			
		];

		dispatch(setData(data));
		dispatch(setLoading(false));

	}
}

export const OnToggleModalModal = (dri) =>{
	return async (dispatch) => {

		dispatch(setModalVisible(dri));
	}
}

export const OnSetTitleModal = (title) =>{
	return async (dispatch) => {

		dispatch(setTitle(title));
	}
}

