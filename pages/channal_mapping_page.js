import React from 'react';
// import LayoutWithSidebar from "../components/LayoutWithSidebar";
import LayoutTopHeader from "../components/LayoutTopHeader";
import ChannalMapping from "../components/ChannalMapping";

const channal_mapping_page = () => {
	return (
		<LayoutTopHeader>
			<ChannalMapping />
		</LayoutTopHeader>
	);
};

export default channal_mapping_page;