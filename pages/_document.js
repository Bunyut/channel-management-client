import Document, { Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render () {
    return (
      <html>
        <Head>
          <meta name='viewport' content='width=device-width, initial-scale=1' />
          <meta charSet='utf-8' />
          <link rel='stylesheet' href='/static/css/antd-mobile.min.css' />
          <link rel='stylesheet' href='/static/css/bootstrap.min.css' />
          <link rel='stylesheet' href='/static/css/nprogress.css' />
          <link rel='stylesheet' href='/static/antd/antd.min.css' />
          <link rel='stylesheet' href='/static/css/custom.css' />
          <link rel="stylesheet" href="/static/css/react-table.css" />
          
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
