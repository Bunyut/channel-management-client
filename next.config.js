const withCSS = require('@zeit/next-css');

const url = 'http://www.snufflehp.com:5000'//'https://localhost:5001'//'http://54.255.240.59:5000';
//const url = 'https://localhost:5001';
module.exports = withCSS({
	publicRuntimeConfig: { // Will be available on both server and client
		//API_URL: 'http://3.1.218.106:5000'//'http://localhost:3001'
		API_URL:url
	},
	env: {
		apiUrl:url
	}
})
