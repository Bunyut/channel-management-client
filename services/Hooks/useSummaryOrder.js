import React, { useState, useEffect } from "react";


const useSummaryOrder = (orders, shippingType, shippingPrice) => {

	const [sumOrderPrice, setSumOrderPrice] = useState(0);
	const [sumShipping, setSumShipping] = useState(0);
	const [sumTotal, setSumTotal] = useState(0);
	const [sumTotalWeight, setSumTotalWeight] = useState(0);

	useEffect(() => {

		let orderPrice = 0;//orders.reduce((acc, curr) => acc + (curr.productPrice * curr.qty), 0);
		let sumWeight = 0;

		for (let index = 0; index < orders.length; index++) {
			const curr = orders[index];
			orderPrice += (curr.productPrice * curr.qty);
			sumWeight += (curr.weight * curr.qty);
		}

		//console.log('--cal--orderPrice--', orderPrice);

		setSumOrderPrice(orderPrice);
		setSumTotalWeight(sumWeight);

		//let weightPrice = calShippingWeight(shippingType)
		
		setSumTotal(orderPrice + sumShipping);

		//console.log(shippingType, shippingPrice, sumTotalWeight);


	}, [orders]);

	useEffect(() => {

		if (shippingType) {

			//let shipPrice = 0;
			// switch (shippingType) {
			// 	case "1":
			// 		shipPrice = 35;
			// 		break;

			// 	case "2":
			// 		shipPrice = 50;
			// 		break;

			// 	default:
			// 		break;
			// }

			
			

			let weightPrice = calShippingWeight(shippingType, shippingPrice, sumTotalWeight);
			//console.log('weightPrice-->',weightPrice);
			
			let shipPrice = (weightPrice) ? (weightPrice.shippingPrice1 + weightPrice.shippingPackingPrice)  :0;
		
			setSumShipping(shipPrice);
			setSumTotal(shipPrice + sumOrderPrice);
		}

	}, [shippingType]);

	const calShippingWeight = (sType,wList,weight) =>{

		//console.log('sType,wList,weight--->', sType,wList,weight);
		//var lists = wList.filter(i =>  (i.shippingId == sType && weight <= i.shippingWeigthMax ) );

		var currType = wList.filter(i =>  i.shippingId == sType );
		currType.sort(function (a, b) {
			return a.shippingWeigthMax - b.shippingWeigthMax;
		});


		//console.log('--currType', currType);
		

		var lists = [];
		var min = 0;
		var max = 0;
		for (let index = 0; index < currType.length-1; index++) {
			
			min = currType[index].shippingWeigthMax;
			max = currType[index+1].shippingWeigthMax;

			//console.log('--->',currType[index], currType[index+1]);
			

			if(min < weight && max >= weight){
				lists.push(currType[index+1]);
			}
			
		}
		
		//console.log(' lists--', lists);
		

		let curr = lists[0];//lists[lists.length - 1]

		//console.log('-->',lists, curr);

		return curr;

	}

	return { sumOrderPrice, sumShipping, sumTotal , sumTotalWeight}
}

export default useSummaryOrder;