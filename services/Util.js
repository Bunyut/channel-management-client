import namor from "namor";
export const dateFormatToThai = dateIn => {
  const RE_DATE = /(?<y>[0-9]{4})\-(?<m>[0-9]{2})\-(?<d>[0-9]{2})(T)*(?<time>[0-9\:]*)/g;

  const matchObj = RE_DATE.exec(dateIn);

  //console.log('matchObj->', matchObj)

  return matchObj !== undefined && matchObj !== null
    ? `${matchObj.groups.d}/${matchObj.groups.m}/${parseInt(
        matchObj.groups.y,
        10
      ) + 543} ${matchObj.groups.time}`
    : "";
};

export const dateOnlyFormatToThai = dateIn => {
  const RE_DATE = /(?<y>[0-9]{4})\-(?<m>[0-9]{2})\-(?<d>[0-9]{2})(T)*(?<time>[0-9\:]*)/g;
  const matchObj = RE_DATE.exec(dateIn);
  //console.log('matchObj->', matchObj)
  return matchObj !== undefined && matchObj !== null
    ? `${matchObj.groups.d}/${matchObj.groups.m}/${parseInt(
        matchObj.groups.y,
        10
      ) + 543}`
    : "";
};

export const getDateOnly = dateTime => {
  const RE_DATE = /(?<y>[0-9]{4})\-(?<m>[0-9]{2})\-(?<d>[0-9]{2})(T)*(?<time>[0-9\:]*)/g;

  const matchObj = RE_DATE.exec(dateTime);

  //console.log('matchObj->', matchObj)

  return matchObj !== undefined && matchObj !== null
    ? `${matchObj.groups.y}-${matchObj.groups.m}-${matchObj.groups.d}`
    : dateTime;
};

export const mapStatus = val => {
  let txt = "";

  switch (val) {
    case 1:
      txt = "ขออนุมัติ";
      break;
    case 2:
      txt = "อนุมัติแล้ว";
      break;
    case 3:
      txt = "ไม่อนุมัติ";
      break;
    case 4:
      txt = "ยกเลิก";
      break;
    case 5:
      txt = "สําเร็จแล้ว";
      break;

    default:
      break;
  }

  return txt;
};

export const mapLevelName = status => {
  let txt = "";
  switch (status) {
    case 1:
      txt = "Junior";
      break;
    case 2:
      txt = "Senior";
      break;
    case 3:
      txt = "Manager";
      break;
    default:
      break;
  }

  return txt;
};


const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
  const statusChance = Math.random();
  return {
    firstName: namor.generate({ words: 1, numbers: 0 }),
    lastName: namor.generate({ words: 1, numbers: 0 }),
    age: Math.floor(Math.random() * 30),
    visits: Math.floor(Math.random() * 100),
    progress: Math.floor(Math.random() * 100),
    status:
      statusChance > 0.66
        ? "relationship"
        : statusChance > 0.33 ? "complicated" : "single"
  };
};

export function makeData(len = 5553) {
  return range(len).map(d => {
    return {
      ...newPerson(),
      children: range(10).map(newPerson)
    };
  });
}

export const Logo = () =>
  <div style={{ margin: '1rem auto', display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center'}}>
    For more examples, visit {''}
  <br />
    <a href="https://github.com/react-tools/react-table" target="_blank">
      <img
        src="https://github.com/react-tools/media/raw/master/logo-react-table.png"
        style={{ width: `150px`, margin: ".5em auto .3em" }}
      />
    </a>
  </div>;

export const Tips = () =>
  <div style={{ textAlign: "center" }}>
    <em>Tip: Hold shift when sorting to multi-sort!</em>
  </div>;
